all:
	g++ -o humma ./src/timantti.cpp -lfluidsynth -ljack -lasound
clean:
	rm -f *.o
	rm -f -r humma
install:
	mkdir -p $(DESTDIR)/usr/bin/
	install -m 0755 humma $(DESTDIR)/usr/bin/
uninstall:
	rm -f $(DESTDIR)/usr/bin/humma
