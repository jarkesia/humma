/* 
	Nero ohjelma soundfonttien soittamiseen maagisesti.
	2009-2012 (C) Jari Suominen
*/

#include <iostream>
#include <fluidsynth.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <fstream>
#include <cstdlib>
#include <string>
#include <list>
#include <getopt.h>
#include <signal.h>
#include <jack/jack.h>

#define JACK_NO_START_SERVER

using namespace std;

const int LL = 512;
const int LAST_MIDI_NOTE = 130;
const int MAX_SFONTS = 20;

const int blacks[] = {1,3,6,8,10,13,15,18,20,22,25,27,30,
		32,34,37,39,42,44,46,49,51,54,56,58,61,63,66,68,
		70,73,75,78,80,82,85,87,90,92,94,97,99,102,104,
		106,109,111,114,116,118,121,123,126,-1};
const int whites[] = {2,4,5,7,9,11,12,14,16,17,19,21,23,24,26,28,29,31,
		33,35,36,38,40,41,43,45,47,48,50,52,53,55,57,59,60,62,64,65,67,69,
		71,72,74,76,77,79,81,83,84,86,88,89,91,93,95,96,98,100,101,103,105,
		107,108,110,112,113,115,117,119,120,122,124,125,-1};

static struct option sfoptions[] = {
        	{"toggle",     no_argument,       0, 't'},
		{"oneshot",    no_argument,       0, 's'},
		{"mono",       no_argument,       0, 'M'},
        	{"mute",       required_argument, 0, 'm'},
		{"unmute",     required_argument, 0, 'u'},
        	{"volume",     required_argument, 0, 'v'},
        	{"panning",    required_argument, 0, 'p'},
		{"random",     required_argument, 0, 'r'},
		{"octave",     required_argument, 0, 'O'},
        	{"offset",     required_argument, 0, 'o'}
	};

fluid_synth_t* synth;
int sfonts_loaded = 0;
ifstream cfg_file;
char cfg_file_path[256];
string sfont_path("");
char str[256];
int program_change = 0;

char * client_name = (char*)"humma";

int loaded_sfont_ids[MAX_SFONTS];
int loaded_sfont_modes[MAX_SFONTS];
int loaded_sfont_vels[MAX_SFONTS];
int loaded_sfont_ofs[MAX_SFONTS];
int loaded_sfont_pans[MAX_SFONTS];
int loaded_sfont_polyphony[MAX_SFONTS];
int loaded_sfont_random_mode[MAX_SFONTS];
int loaded_sfont_random_range[MAX_SFONTS][2];

const int MODE_NORMAL = 0;
const int MODE_TOGGLE = 1;
const int MODE_ONE_SHOT = 2;
const int POLY = 0;
const int MONO = 1;
const int ON = 1;
const int OFF = 0;

bool note_on[LAST_MIDI_NOTE]; // This is significant only to soundfonts with toggle mode on!
int midi_through[LAST_MIDI_NOTE];

const int KEY_UP = 0;
const int KEY_DOWN = 1;
const int NOTE_ON = 1;
const int NOTE_OFF = 0;

int midiin = 0;
int midiout = 0;

bool running = false;
bool zombified = false;

list<int> keys_pressed;

bool note_active[MAX_SFONTS][LAST_MIDI_NOTE];

bool string2int(char*d,int&r){for(r=0;*d>='0'&&*d<='9';r=r*10+*(d++)-'0');return*d==0;}

void exit_cli(int sig) {
	running = false;
	std::cerr << "\r    \nSuljemme...\n";
}

/*
	Reads integer from the config file that hopefully is open.
*/
int readInt() {
	int retval = 0;
	char * a = strtok(NULL," ");
	if (a[0]=='-'){
		string2int(&a[1],retval);
		retval = retval*-1;
	} else {
		string2int(a,retval);
	}
	return retval;
}

int readString() {
	int retval = 0;
	char * a = strtok(NULL," ");
	if (a[0]=='-'){
		string2int(&a[1],retval);
		retval = retval*-1;
	} else {
		string2int(a,retval);
	}
	return retval;
}


/*
	This is printing debug data to the screen when synth is being played.
	Add psychedelic action here later. Not significant function though.

*/
void print_keys_pressed() {
	if (keys_pressed.empty()) {
		cout << "." << endl;
		return;
	}
	list<int>::iterator i = keys_pressed.begin();
	while(i!=keys_pressed.end()) {
		cout << *i << " ";
		i++;
	}
	cout << endl;
}

/*
	Initializing ALSA MIDI.
	It is highly unlikely that you want to edit this or even check what it has eaten.
 */
snd_seq_t *open_seq() {

  snd_seq_t *seq_handle;
  int portid;

  if (snd_seq_open(&seq_handle, "hw", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
    fprintf(stderr, "Error opening ALSA sequencer.\n");
    exit(1);
  }
  snd_seq_set_client_name(seq_handle, client_name);
  if ((midiin = snd_seq_create_simple_port(seq_handle, "MIDI in",
            SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
            SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating sequencer port.\n");
    exit(1);
  }
  if ((midiout = snd_seq_create_simple_port(seq_handle, "MIDI out",
            SND_SEQ_PORT_CAP_READ|SND_SEQ_PORT_CAP_SUBS_READ,
            SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating sequencer port.\n");
    exit(1);
  }
  return(seq_handle);
}

/*
	Unloading soundfonts from the the memory.
*/
void unload_sfonts() {
	std::cout << "Unloading soundfonts ...";
	fluid_synth_system_reset(synth);
	while (sfonts_loaded > 0) {
		sfonts_loaded--;
		fluid_synth_sfunload(synth,loaded_sfont_ids[sfonts_loaded],0);
		loaded_sfont_vels[sfonts_loaded] = -1;
		loaded_sfont_ofs[sfonts_loaded] = 0;
		loaded_sfont_modes[sfonts_loaded] = MODE_NORMAL;
		loaded_sfont_pans[sfonts_loaded] = 0;
		loaded_sfont_polyphony[sfonts_loaded] = POLY;
		loaded_sfont_random_mode[sfonts_loaded] = OFF;
	}
	for (int i =0; i < LAST_MIDI_NOTE; i++) {
		note_on[i] = false;
	}
	std::cout << "done.\n";
}

void update_mute_range(char * a, bool on) {
	int start = 0;
	int end = 0;
	if (a[0]=='w') {
		std::cout << "white keys.\n";
		int prev = 0;
		for (int i = 0; whites[i]!=-1; i++) {
			note_active[sfonts_loaded][whites[i]] = on;
		}
	} else if (a[0]=='b') {
		std::cout << "black keys.\n";
		for (int i = 0; blacks[i]!=-1; i++) {
			note_active[sfonts_loaded][blacks[i]] = on;
		}
	} else if (a[0]=='a') {
		std::cout << "all keys.\n";
		for (int i = 0; i <= LAST_MIDI_NOTE; i++) {
			note_active[sfonts_loaded][i] = on;
		}
	} else { // numeroita ineen
		//printf("halo: %s\n",a);
		char * c;
		string2int(strtok(a,":"),start);
		std::cout << "range from " << start << " to ";
		
		c = strtok(NULL,":");
		if (c!=NULL) {
			string2int(c,end);
		} else {
			end = start;
		}
		//a = strtok(NULL," ");
		//string2int(a,end);
		std::cout << end << ".\n";
		for (int i = start; i <= end; i++) {
			note_active[sfonts_loaded][i] = on;
		}
	}
}

void update_sfont() {
	int i = 0;
	char * sfontparam[256];
	char * a;

	for (int j = 0; j < 256; j++) {
		sfontparam[j] = NULL;
	} 

	// modifying sfont line to form digested by getopt
	a = strtok (str," ");
	while (a != NULL) {
		sfontparam[i] = a;	
		a = strtok(NULL," ");
		//printf("\t\t%i %s\n",i,sfontparam[i]);
		i++;
		
	}

	// loading sfont
	char sf[256] = "";
	strcat(sf,sfont_path.c_str());
	strcat(sf,sfontparam[0]);
	std::cout << "Loading " << sf << " ...";
	loaded_sfont_ids[sfonts_loaded] = fluid_synth_sfload(synth, sf, 0);
	if (loaded_sfont_ids[sfonts_loaded]==-1) {
		std::cout << "error. Loading aborted. Check the path.\n";
	} else {
		std::cout << "loaded.\n";
		fluid_synth_program_select(synth, sfonts_loaded, loaded_sfont_ids[sfonts_loaded], 0, 0);
		for (int i = 0; i < LAST_MIDI_NOTE; i++) {
			note_active[sfonts_loaded][i] = true;
		}
	}

	// updating options:
	int option_index = 0, opt=0;
	optind = 0; // resets getopt between calls

	while(1) {
        	opt = getopt_long (i, sfontparam, "v:tsm:p:o:Mr:O:u:",
                           sfoptions, &option_index);
		//printf("opt: %i %i %i\n",opt,i,option_index);
        	if (opt==-1) break;
        	switch (opt) {
        		case '0': break;
        		case 't':
				loaded_sfont_modes[sfonts_loaded] = MODE_TOGGLE;
				std::cout << "\tToggle mode activated.\n";
            			break;
			case 's':
            			loaded_sfont_modes[sfonts_loaded] = MODE_ONE_SHOT;
				std::cout << "\tOne shot mode activated.\n";
            			break;
			case 'm':
            			std::cout << "\tMuteing ";
				update_mute_range(optarg,false);
            			break;
			case 'u':
            			std::cout << "\tUnmuteing ";
				update_mute_range(optarg,true);
            			break;
        		case 'v':
				float f;
				loaded_sfont_vels[sfonts_loaded] = (int)strtol(optarg,NULL,10);
				if (loaded_sfont_vels[sfonts_loaded] > 100 || loaded_sfont_vels[sfonts_loaded] < 0) {
					std::cout << "\tConstant velocity should be between 0 and 100! You tried to set it to " << loaded_sfont_vels[sfonts_loaded] << "!\n";
					std::cout << "\tWalk on home boy!\n";
					exit(0);
				}
				// http://www.dr-lex.be/info-stuff/volumecontrols.html
				f = loaded_sfont_vels[sfonts_loaded]/100.0;
				f = f*f;			
				loaded_sfont_vels[sfonts_loaded] = (int)(f*100);
				std::cout << "\tConstant velocity " << loaded_sfont_vels[sfonts_loaded] << " set.\n";
            			break;
        		case 'p':
				int v;
				if (strlen(optarg)==1) {
					if (optarg[0]=='l') {
						v = -100;
					} else if (optarg[0] == 'r') {
						v = 100;
					}
				} else {
					v = (int)strtol(optarg,NULL,10);
				}
				std::cout << "\tPanning all sounds to " << v;
				loaded_sfont_pans[sfonts_loaded] = (v+100)*0.635;	
				fluid_synth_cc(synth,sfonts_loaded,10,loaded_sfont_pans[sfonts_loaded]);
				std::cout << ".\n";
            			break;
			case 'o':
				loaded_sfont_ofs[sfonts_loaded] = (int)strtol(optarg,NULL,10);
				std::cout << "\tOffset of " << loaded_sfont_ofs[sfonts_loaded] << " midi notes set.\n";
            			break;
			case 'M':
				std::cout << "\tMono mode."<< endl;
				loaded_sfont_polyphony[sfonts_loaded] = MONO;
            			break;
			case 'r':
				std::cout << "\tRandom sample mode on ";
				char * c;
				int start, end;
				string2int(strtok(optarg,":"),start);
				std::cout << "range from " << start << " to ";
				c = strtok(NULL,":");
				if (c!=NULL) {
					string2int(c,end);
				} else {
					end = start;
				}
				loaded_sfont_random_range[sfonts_loaded][0] = start;
				loaded_sfont_random_range[sfonts_loaded][1] = end;
				loaded_sfont_random_mode[sfonts_loaded] = ON;
				std::cout << end << ".\n";
				//for (int j = 1; j <= LAST_MIDI_NOTE; j++) {
				//	note_active[sfonts_loaded][j] = false;
				//}
				//note_active[sfonts_loaded][60] = true;
            			break;
        	}
    	}
	sfonts_loaded++;

}

/*
	Processes first line on the bank section at the config file.
	This line includes the list of soundfonts that should be loaded.
*/
void update_sfonts() {
	unload_sfonts();
	cfg_file.getline(str,LL);
	while (str[0]!='#') {
		update_sfont();
		cfg_file.getline(str,LL);
	}
	std::cout << "Loaded " << sfonts_loaded << " soundfonts.\n";
}

void update_function_keys() {
	std::cout << "Updating funtion key parameters: ";
	
	char * a;
	a = strtok (str," ");
	for (int i = 0; i < LAST_MIDI_NOTE; i++) {
		midi_through[i] = -1;
	}

	while (a != NULL) {
		if (a[0]=='n') {
			a = strtok(NULL," ");
			string2int(a,program_change);
			std::cout << "Pressing midinote " << program_change << " will load next bank.\n";
		} if (a[0]=='t') { // midi through
			int start,end,o_set;
			a = strtok(NULL," ");
			string2int(a,start);
			a = strtok(NULL," ");
			string2int(a,end);
			a = strtok(NULL," ");
			string2int(a,o_set);
			for (int i = start; i <= end; i++) {
				midi_through[i] = i+o_set;
			}
			std::cout << "Range from " << start << " to " << end << " connected to MIDI through ";
			std::cout << "and added offset of " << o_set << " .\n";
			
		} else {
			a = strtok(NULL," ");
		}
	}
}

void load_next_bank() {
	std::cout << "\n\nLOADING NEW BANK\n";
	if (cfg_file.is_open()) {
		if (cfg_file.eof()) {
			std::cout << "Tillbaks till ruta ett igen.\n";
			cfg_file.close();
		} else {
			cfg_file.getline(str,LL);
			if (str[0]=='*') {
				std::cout << "Tillbaks till ruta ett igen.\n";
				cfg_file.close();
			}
		}
	}
	if (!cfg_file.is_open()) {
		std::cout << "Opening configuration file " << cfg_file_path << " ...";
		cfg_file.open(cfg_file_path);
		if(!cfg_file) {
			std::cout << "error! You killed me. Check that the path is correct.\n";
    			exit(1);
  		} else {
			std::cout << "done.\n";
			cfg_file.getline(str,LL);
		}
	}
	//cfg_file.getline(str,256); // this line should only have #
	update_function_keys();
	update_sfonts();
}

void note_onn(int key, int vel, int font) {
	if (loaded_sfont_modes[font]!=MODE_TOGGLE || note_on[key]==false) {
		if (loaded_sfont_vels[font]!=-1) {
			if (loaded_sfont_random_mode[font] == ON) {
				int n = (random()*1.0/RAND_MAX)*(loaded_sfont_random_range[font][1]-loaded_sfont_random_range[font][0]+1)+loaded_sfont_random_range[font][0];
				fluid_synth_noteon(synth, font, n, loaded_sfont_vels[font]);
			} else if ((key + loaded_sfont_ofs[font])<128 && (key+loaded_sfont_ofs[font]) > -1) {
				fluid_synth_noteon(synth, font, key + loaded_sfont_ofs[font], loaded_sfont_vels[font]);
			}
		} else if ((key + loaded_sfont_ofs[font])<128 && (key+loaded_sfont_ofs[font]) > -1) {
			fluid_synth_noteon(synth, font, key + loaded_sfont_ofs[font], vel);
		}
	} else if ((key + loaded_sfont_ofs[font])<128 && (key+loaded_sfont_ofs[font]) > -1) { // toggle mode is on and we are toggling the sample off
		fluid_synth_noteoff(synth, font, key + loaded_sfont_ofs[font]);
	}	
}

void note_off(int key) {
	for (int i = 0; i < sfonts_loaded; i++) {
		if (loaded_sfont_polyphony[i]==MONO) {
			
			fluid_synth_noteoff(synth, i, key + loaded_sfont_ofs[i]);
			list<int>::iterator o = keys_pressed.begin();
			while (!note_active[i][*o] || *o == key) {
				if (o!=keys_pressed.end()) {
					o++;
				} else {
					break;
				}
			}
			if (*o < key && note_active[i][*o] && *o != key) {
				note_onn(*o,loaded_sfont_vels[i],i);
			}			
		} else if (loaded_sfont_modes[i] == MODE_NORMAL) {
			if (loaded_sfont_random_mode[i] == ON) {
				for (int k = loaded_sfont_random_range[i][0]; k <= loaded_sfont_random_range[i][1]; k++) {
					fluid_synth_noteoff(synth, i, k);
				}
			} else if ((key + loaded_sfont_ofs[i])<128 && (key+loaded_sfont_ofs[i]) > -1) {
				fluid_synth_noteoff(synth, i, key + loaded_sfont_ofs[i]);
			}
		}
	}
	keys_pressed.remove(key);
	print_keys_pressed();
	return;
}

/*
	This function processes the MIDI messages arriving from MIDI keyboard through ALSA MIDI.
	Only noteon and noteoff messages are used!
 */
void midi_action(snd_seq_t *seq_handle) {
	snd_seq_event_t *ev;

	do {
		snd_seq_event_input(seq_handle, &ev);
		switch (ev->type) {
			case SND_SEQ_EVENT_NOTEON:
				if ((int)ev->data.note.note == program_change && ev->data.note.velocity!=0) {
					if (keys_pressed.size() == 0) { // PRESET CHANGE BRANCH
						load_next_bank();
					}
					return;
				}
				if (midi_through[(int)ev->data.note.note]!=-1) { // MIDI THROUGH BRANCH
					snd_seq_ev_set_subs(ev);
					snd_seq_ev_set_direct(ev);
					ev->data.note.note = midi_through[ev->data.note.note];
					snd_seq_ev_set_source(ev, midiout);
					snd_seq_event_output_direct(seq_handle,ev);
					return;
				} 
				if (ev->data.note.velocity==0) { // = NOTE_OFF
					note_off(ev->data.note.note);
					return;
				}
				for (int i = 0; i < sfonts_loaded; i++) { // TRUE NOTE ON!
					if (note_active[i][ev->data.note.note]) {
					
						if (loaded_sfont_polyphony[i]==MONO) {
							if (keys_pressed.size()==0) {
								note_onn(ev->data.note.note,ev->data.note.velocity,i);
							} else {
								list<int>::iterator o = keys_pressed.begin();
								while (!note_active[i][*o]) {
									if (o!=keys_pressed.end()) {
										o++;
									} else {
										break;
									}
								}
								if (*o < ev->data.note.note) {
									note_onn(ev->data.note.note,ev->data.note.velocity,i);
									fluid_synth_noteoff(synth, i, *o + loaded_sfont_ofs[i]);
								}
							}
							
						} else {						
							note_onn(ev->data.note.note,ev->data.note.velocity,i);
						}
					}
				}
				keys_pressed.push_back(ev->data.note.note);
				keys_pressed.sort();
				keys_pressed.unique();
				keys_pressed.reverse();
				print_keys_pressed();
				note_on[ev->data.note.note] = !note_on[ev->data.note.note];
				break;        
			case SND_SEQ_EVENT_NOTEOFF:
				if (midi_through[(int)ev->data.note.note]!=-1) {
					snd_seq_ev_set_subs(ev);
					snd_seq_ev_set_direct(ev);
					ev->data.note.note = midi_through[ev->data.note.note];
					snd_seq_ev_set_source(ev, midiout);
					snd_seq_event_output_direct(seq_handle,ev);
					return;
				} 
				note_off(ev->data.note.note);
				break;       
			case SND_SEQ_EVENT_PITCHBEND: 
				for (int i = 0; i < sfonts_loaded; i++) {
					fluid_synth_pitch_bend(synth, i, (int)ev->data.control.value + 8192);
				}
				std::cout << "pitch bend " << (int)ev->data.control.value << ".\n";
				break;
			case SND_SEQ_EVENT_CONTROLLER: 
				for (int i = 0; i < sfonts_loaded; i++) {
					fluid_synth_cc(synth, i, ev->data.control.param, ev->data.control.value);
				}
				std::cout << "controller " << (int)ev->data.control.param << " value " << (int)ev->data.control.value << ".\n";
				break;
			case SND_SEQ_EVENT_PORT_SUBSCRIBED:
				/*fprintf(stderr,"Port subscribed: sender: %d:%d dest: %d:%d\n",
                 ev->data.connect.sender.client,
                 ev->data.connect.sender.port,
                 ev->data.connect.dest.client,
                 ev->data.connect.dest.port);*/
				break;
			case SND_SEQ_EVENT_PORT_UNSUBSCRIBED:
				/*fprintf(stderr,"Port unsubscribed: sender: %d:%d dest: %d:%d\n",
                 ev->data.connect.sender.client,
                 ev->data.connect.sender.port,
                 ev->data.connect.dest.client,
                 ev->data.connect.dest.port);*/
				break;
			default:
				std::cout << "hmm... " << (int)ev->type << " event received." << endl;
				break;
   	}
		snd_seq_free_event(ev);
 	} while (snd_seq_event_input_pending(seq_handle, 0) > 0);
}

void jack_zombified (void *arg) {
		cout << "Humma thinks Jack server has crashed!..." << endl;
		zombified = true;
		running = false;
}


/*
	Here is all the business happening.
 */
int main(int argc, char* argv[]) {
	std::cout << "Humma started welcome to spychedelic explotion!\n2011 (C) Jari Suominen\n";

	if (argv[1]==NULL) {
		std::cout << "Config file not specified. Exiting.\n";
		exit(1);	
	}

	strncpy(cfg_file_path,argv[1],256);
	
	char * pch;
	pch = strtok (argv[1],"/");
	string h("");
	if (strlen(argv[1])-strlen(pch)==1) {
		sfont_path = "/";
	}
	while (pch != NULL) {
		h = pch;
		pch = strtok (NULL, "/");
		if (pch != NULL) {
			sfont_path = sfont_path + h + "/";
		}
	}
		
	std::cout << sfont_path << "\n";
	std::cout << cfg_file_path << "\n";

	if (argv[2] != NULL) {
		cout << "Setting client name to '" << argv[2] << "'." << endl;
		client_name = argv[2];
	}


	/* Jack watching mock client
		This bit is set because of the implementation of libfluidsynth that
		gives you no way of knowing if the jackd has crashed. So we'll just
		create one client that does not do anything, but is informed when 
		jackd crashed/closes. We can also use this to figure out the sample-rate
		of which jack is running.
	*/
	const char *zombie_name = "zombie";
	const char *server_name = NULL;
	jack_options_t options = JackNoStartServer;
	jack_status_t status;

	jack_client_t * client = jack_client_open (zombie_name, options, &status, server_name);
	if (client == NULL) {
		fprintf (stderr, "jack_client_open() failed, "
			 "status = 0x%2.0x\n", status);
		if (status & JackServerFailed) {
			fprintf (stderr, "Unable to connect to JACK server\n");
		}
		exit (1);
	}
	if (status & JackNameNotUnique) { // jack will give unique names for the clients automatically.
		zombie_name = jack_get_client_name(client);
	}
	jack_on_shutdown (client, jack_zombified, 0);

	if (jack_activate (client)) {
		fprintf (stderr, "cannot activate client");
		exit (1);
	}


	/* This bit is for setting up FLUIDSYNTH engine */
	fluid_settings_t* settings;
	fluid_audio_driver_t* adriver;

	settings = new_fluid_settings();
	fluid_settings_setint(settings, (char*)"synth.midi-channels", 32);
	cout << "Humma is running at " << jack_get_sample_rate (client) << endl;
	fluid_settings_setnum(settings, (char*)"synth.sample-rate", jack_get_sample_rate (client));
	synth = new_fluid_synth(settings);
	//fluid_settings_setstr(settings, (char*)"audio.jack.id", (char*)"humma");
	fluid_settings_setstr(settings, (char*)"audio.jack.id", client_name);
	fluid_settings_setstr(settings, (char*)"audio.driver", (char*)"jack");

	//fluid_settings_setstr(settings, "synth.reverb.active", "yes");
	adriver = new_fluid_audio_driver(settings, synth);

	/* This bit is for setting up Alsa MIDI */
	snd_seq_t *seq_handle;
	int npfd;
	struct pollfd *pfd;

	seq_handle = open_seq();
	npfd = snd_seq_poll_descriptors_count(seq_handle, POLLIN);
	pfd = (struct pollfd *)alloca(npfd * sizeof(struct pollfd));
	snd_seq_poll_descriptors(seq_handle, pfd, npfd, POLLIN);


	/* This bit it for initializing humma engine */
	for (int i = 0; i < MAX_SFONTS; i++) {
		loaded_sfont_vels[i] = -1;
		loaded_sfont_modes[i] = MODE_NORMAL;
		loaded_sfont_ofs[i] = 0;
		loaded_sfont_pans[i] = 0;
		loaded_sfont_polyphony[i] = POLY;
		loaded_sfont_random_mode[i] = OFF;
	}

	for (int i = 0 ; i < LAST_MIDI_NOTE; i++) {
		note_on[i] = false;
		midi_through[i] = -1;
	}

	load_next_bank();

	/* reverb */
	/*fluid_synth_set_reverb_on(synth, 1);*/
	//fluid_synth_set_vibrato_on(synth, 1);

	running = true;
	signal(SIGINT, exit_cli);
	signal(SIGTERM, exit_cli);

	while (running) {
		if (poll(pfd, npfd, 10) > 0) {
			midi_action(seq_handle);
		}
	}
	
//fluid_jack_audio_driver_t* dev = (fluid_jack_audio_driver_t*) adriver;

//std::cout << "ADRIVER" << adriver << " " << (adriver==NULL) << "\n";

//sleep(1000);

	// DELETING EVERYTHING SMOOTHLY
	if (!zombified) {
		delete_fluid_audio_driver(adriver);
		jack_client_close (client);
	}
	
	delete_fluid_synth(synth);
	delete_fluid_settings(settings);
	snd_seq_close(seq_handle);

	std::cout << "... Morjens!\n";
	return 0;
}

